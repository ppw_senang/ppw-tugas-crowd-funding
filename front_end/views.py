from django.shortcuts import render
from program.models import Berita,Program
from django.contrib import messages
# Create your views here.

def dashboard(request):
    return render(request, 'dashboard.html')

def profile(request):
	if request.user.is_authenticated:
		return render(request, 'profile.html')
	else:
		messages.error(request, "Mohon login terlebih dahulu")
		return render(request, 'login.html', {})

# Dari gema
def news1(request):
    return render(request, 'news1.html')

def news2(request):

    return render(request, 'news2.html')

def news3(request):

    return render(request, 'news3.html')

def news4(request):

    return render(request, 'news4.html')

def news5(request):

    return render(request, 'news5.html')

def news6(request):

    return render(request, 'news6.html')
