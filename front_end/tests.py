from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from front_end.apps import FrontEndConfig
from .views import *

class TDDTest(TestCase):
    def test_front_end_url_is_exist(self):
         response = Client().get('')
         self.assertEqual(response.status_code,200)

    def test_front_end_using_to_do_list_template(self):
         response = Client().get('')
         self.assertTemplateUsed(response, 'dashboard.html')

    def test_front_end_profile_url_is_exist(self):
        response = Client().get('/profil/')
        self.assertEqual(response.status_code,200)

    def test_front_end_profile_using_to_do_list_template_redirect(self):
         response = Client().get('/profil/')
         self.assertTemplateUsed(response, 'login.html')

    def test_front_end_news1_url_is_exist(self):
        response = Client().get('/news1/')
        self.assertEqual(response.status_code,200)

    def test_front_end_news1_using_to_do_list_template(self):
         response = Client().get('/news1/')
         self.assertTemplateUsed(response, 'news1.html')

    def test_front_end_news2_url_is_exist(self):
        response = Client().get('/news2/')
        self.assertEqual(response.status_code,200)

    def test_front_end_news2_using_to_do_list_template(self):
         response = Client().get('/news2/')
         self.assertTemplateUsed(response, 'news2.html')

    def test_front_end_news3_url_is_exist(self):
        response = Client().get('/news3/')
        self.assertEqual(response.status_code,200)

    def test_front_end_news3_using_to_do_list_template(self):
         response = Client().get('/news3/')
         self.assertTemplateUsed(response, 'news3.html')

    def test_front_end_news4_url_is_exist(self):
        response = Client().get('/news4/')
        self.assertEqual(response.status_code,200)

    def test_front_end_news4_using_to_do_list_template(self):
         response = Client().get('/news4/')
         self.assertTemplateUsed(response, 'news4.html')

    def test_front_end_news5_url_is_exist(self):
        response = Client().get('/news1/')
        self.assertEqual(response.status_code,200)

    def test_front_end_news5_using_to_do_list_template(self):
         response = Client().get('/news5/')
         self.assertTemplateUsed(response, 'news5.html')

    def test_front_end_news6_url_is_exist(self):
        response = Client().get('/news6/')
        self.assertEqual(response.status_code,200)

    def test_front_end_news6_using_to_do_list_template(self):
         response = Client().get('/news6/')
         self.assertTemplateUsed(response, 'news6.html')

    def test_apps(self):
        self.assertEqual(FrontEndConfig.name, 'front_end')
        self.assertEqual(apps.get_app_config('front_end').name, 'front_end')