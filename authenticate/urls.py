#import auth django urls.py
from django.urls import include ,path
from authenticate import auth_views as views

urlpatterns = [
    path('daftar/' , views.signup , name="sign_up" ),
    path('masuk/', views.login_views , name="login"),
    path('keluar/',views.logout_views , name="logout")
]
