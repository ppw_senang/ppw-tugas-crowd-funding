from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _

# Create your models here.
class CrowdFundingUser(AbstractUser):
    nama = models.CharField(max_length=100)
    tanggal_lahir = models.DateField(null=True)
    email = models.EmailField(max_length=150, unique=True)
    password = models.CharField(max_length=101)