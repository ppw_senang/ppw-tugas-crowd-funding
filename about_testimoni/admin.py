from django.contrib import admin
from .models import Comment_Model

# Register your models here.
class CommentModelsAdmin(admin.ModelAdmin): # pragma: no cover
    list_display = ('user_comment', 'comment' , 'created_at')
    list_display_links = ('comment',)

admin.site.register(Comment_Model,CommentModelsAdmin) # pragma: no cover