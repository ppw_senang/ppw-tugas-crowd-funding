from django.apps import AppConfig


class AboutTestimoniConfig(AppConfig):
    name = 'about_testimoni'
