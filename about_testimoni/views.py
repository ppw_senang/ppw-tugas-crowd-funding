from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from django.contrib import messages
from .models import Comment_Model
from .forms import Comment_Form
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AdminPasswordChangeForm, PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.utils import timezone

# Create your views here.

# POST method add_message

def aboutUs(request):
	
	if request.method=="POST":
		comment = request.POST["comment"]
		created_at = timezone.now()
		if (request.user.first_name != ""):
			user_comment = request.user.first_name + " " + request.user.last_name
		else:
			user_comment = request.user.nama
		form = Comment_Model(comment=comment , created_at=created_at , user_comment=user_comment)
		form.save()
		return redirect('/tentang_kami/')

	else:
		form = Comment_Form()
		comment_list = Comment_Model.objects.all().order_by('-created_at')
		paginator = Paginator(comment_list, 5)
		page = request.GET.get('page')
		comment_paginate = paginator.get_page(page)

		return render(request, "aboutUs.html", {'form': form, 'comment_paginate': comment_paginate})
