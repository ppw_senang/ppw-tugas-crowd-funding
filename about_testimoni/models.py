from django.db import models
from django.utils import timezone

# Create your models here.
class Comment_Model(models.Model):
	user_comment = models.CharField(max_length=300 ,  default='Anonim')
	comment = models.CharField(max_length=300)
	created_at = models.DateTimeField(auto_now=True)