from .forms import Comment_Form
from django.test import TestCase, Client
from django.urls import resolve
from about_testimoni import views
from .views import *
from django.utils import timezone


class FormTest(TestCase):

    def test_form_url_is_exist(self):
        response = Client().get('/tentang_kami/')
        self.assertEqual(response.status_code, 200)

    def test_form_using_function(self):
        get_func = resolve('/tentang_kami/')
        self.assertEqual(get_func.func, views.aboutUs)

    # check create status message
    def test_form_create_model(self):
        create_comment = Comment_Model.objects.create(
            comment="sugoi gan", created_at=timezone.now())
        counting_all_messages = Comment_Model.objects.all().count()
        self.assertEqual(counting_all_messages, 1)
