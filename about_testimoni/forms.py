from django import forms
from django.db import models
from django.forms import ModelForm,fields
from .models import Comment_Model

class Comment_Form(forms.ModelForm):

    class Meta:
        model = Comment_Model
        fields = ['comment','user_comment']
        widgets = {
            'user_comment': forms.HiddenInput(),
            'comment': forms.TextInput(attrs={'id':'comment_id', 'class': 'form-control', 'placeholder': 'Beri Komentar anda disini...', 'maxlength':300}),
            'created_at': forms.HiddenInput() #hidden input
        }
