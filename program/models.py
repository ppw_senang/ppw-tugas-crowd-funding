from django.db import models

class Program(models.Model):
    nama_program      = models.CharField(max_length = 200)
    link_gambar       = models.TextField()
    deskripsi_program = models.TextField()
    # nama_donatur      = models.CharField(max_length = 200)
    uang_donasi = models.IntegerField(default = 0)

class Berita(models.Model):
    judul_berita       = models.CharField(max_length = 200)
    link_gambar_berita = models.TextField()
    deskripsi_berita   = models.TextField()

# relational database user dengan program
class DonasiProgramUser(models.Model):
    nama_program = models.CharField(max_length = 200) 
    nama = models.CharField(max_length=26, null=True)
    email = models.EmailField(max_length=150, null=True)
    uang_donasi = models.IntegerField(default = 0)
    nama_donatur = models.CharField(max_length=100)
    
    def __str__(self):
         return self.nama_program