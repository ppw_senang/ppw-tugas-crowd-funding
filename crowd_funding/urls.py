"""DjangoWeb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.conf.urls import url
from program import views as program_views  # ga yakin
from oauth import views as oauth_views # oauth views google

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('front_end.urls')),
    path('', include('authenticate.urls')),
    path('', include('about_testimoni.urls')),
    path('program', program_views.index_list_program, name="program"),
    path('news', program_views.index_list_berita, name="berita"),
    path('donate', program_views.index_donate, name="donate"),
    path('userdonations', program_views.show_user_donations, name = "userdonations"),
    url(r'^auth/', include('social_django.urls', namespace='social')),  # <- Here
]